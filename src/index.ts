import * as createFastify from "fastify";

process.env.FASTIFY_PRETTY_LOGS = "true";

const fastify = createFastify({
  logger: {
    prettyPrint: true
  }
});

fastify.get("/", async (request, response) => {
  return {
    hello: "world"
  };
});

const start = async () => {
  try {
    await fastify.listen(3000);
  } catch (error) {
    fastify.log.error(error);

    process.exit(1);
  }
};

start();
